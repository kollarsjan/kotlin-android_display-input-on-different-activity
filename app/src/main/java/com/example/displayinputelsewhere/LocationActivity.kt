package com.example.displayinputelsewhere

import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_location.*
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices

class LocationActivity : AppCompatActivity() {
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED ) {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    val loc = location?.latitude.toString() + " - " + location?.longitude.toString()
                    textLoc.text = loc
                }
        }
    }
}