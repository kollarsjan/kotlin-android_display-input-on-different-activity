package com.example.displayinputelsewhere

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_MESSAGE: String = "com.example.displayinputelsewhere.MESSAGE"
    }

    private lateinit var sensorManager: SensorManager
    private var lightSensor: Sensor? = null
    private var accelSensor: Sensor? = null
    private var MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
        accelSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

        button_send.setOnClickListener {
            val intent = Intent(this, DisplayMessageActivity::class.java)
            val message: String = edit_message.text.toString()
            intent.putExtra(EXTRA_MESSAGE, message)
            startActivity(intent)
        }

        if (accelSensor != null) {
            button_accel.isEnabled = true
            button_accel.setOnClickListener {
                startActivity(Intent(this, AccelActivity::class.java))
            }
        }

        button_touch.setOnClickListener {
            startActivity(Intent(this, TouchActivity::class.java))
        }

        if (lightSensor != null) {
            button_light.isEnabled = true
            button_light.setOnClickListener {
                startActivity(Intent(this, LightActivity::class.java))
            }
        }

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION )
            == PackageManager.PERMISSION_GRANTED ) {
            activateLocationActivity()
        } else {
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    activateLocationActivity()
                } else {
                    // permission denied, boo!
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun activateLocationActivity() {
        button_location.isEnabled = true
        button_location.setOnClickListener {
            startActivity(Intent(this, LocationActivity::class.java))
        }
    }

}