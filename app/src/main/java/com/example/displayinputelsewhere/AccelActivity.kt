package com.example.displayinputelsewhere

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_accel.*


class AccelActivity : AppCompatActivity(), SensorEventListener {
    private lateinit var sensorManager: SensorManager
    private var accelSensor: Sensor? = null

    private var mData = FloatArray(3)           // Magnetometer
    private var orientation = FloatArray(3)
    private var Rmat = FloatArray(9)
    private var R2 = FloatArray(9)
    private var Imat = FloatArray(9)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Accelerate!!!"
        setContentView(R.layout.activity_accel)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
        // Do something here if sensor accuracy changes.
    }

    override fun onSensorChanged(event: SensorEvent?) {
        textX.text = event!!.values[0].toInt().toString()
        textY.text = event.values[1].toInt().toString()
        textZ.text = event.values[2].toInt().toString()
    }

    override fun onResume() {
        super.onResume()
        accelSensor?.also { accel ->
            sensorManager.registerListener(this, accel,
                SensorManager.SENSOR_DELAY_NORMAL)
        }
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }
}
