package com.example.displayinputelsewhere

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import kotlinx.android.synthetic.main.activity_touch.*

class TouchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Can't touch this"
        setContentView(R.layout.activity_touch)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        textX.setText(event.x.toString())
        textY.setText(event.y.toString())
        return super.onTouchEvent(event)
    }
}
